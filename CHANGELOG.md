# Changes in version 1.4.5 - 2022-03-31
  * Medium changes:
    - Add the blocklist param to the relay.js model
    - Show when bridge support ipv6 on relay search
    - Add stacked graph for V3 network fractions and V2 network fractions

  * Minor changes:
    - Replace Burma country name with Myanmar

# Changes in version 1.4.4 - 2021-12-10

  * Medium changes
     - Fix a few bugs in the overload general timestamp display
     - Edit onionoo protocol specs to add documentation for new parameters
  * Minor changes
     - Fix broken style checks

# Changes in version 1.4.3 - 2021-11-22

  * Medium changes
     - Add endpoint in jetty to expose prometheus service and data exporters
     - Add timestamp as human readable date display on relay search for the
       overload general field. (https://gitlab.torproject.org/tpo/network-health/metrics/relay-search/-/issues/40010)
     - Fix documentation for torperf, by addressing that q1, md etc. are in
       seconds not milliseconds. (https://gitlab.torproject.org/tpo/network-health/metrics/website/-/issues/40008)
     - Fix the note below the onionperf performace graph with the new throughput
       metrics information. (https://gitlab.torproject.org/tpo/network-health/metrics/website/-/issues/40029)

# Changes in version 1.4.2 - 2021-09-28

 * Minor changes
    - Fix null comparison causing a weird bug for some relays appearing
      overloaded when they were not.
    - Add amber dot also in relay search.

# Changes in version 1.4.1 - 2021-09-21

  * Medium changes
    - Fix how v3 statistics are extrapolated. See:
      https://gitlab.torproject.org/tpo/network-health/metrics/statistics/-/issues/40002#note_2745443

  * Minor changes
    - Fix a few copy paste issues in relay search overloaded banner

# Changes in version 1.4.0 - 2020-09-20

 * Medium changes
   - Improve runtime performance of the hidserv module by storing
     extrapolated statistics even if computed network fractions are
     zero, to avoid re-processing these statistics over and over.
   - Extract directory authority bytes per day in the bwhist module.
   - Rewrite insert_bwhist in SQL to improve performance of the bwhist
     module.
   - Estimate relay users by country based on responses to directory
     requests to reduce the overall effect of binning and to make
     relay and bridge user estimates more comparable.
   - Estimate bridge users by country based on requests by country, if
     available, to get more accurate numbers than those obtained from
     unique IP address counts.
   - Update to metrics-lib 2.19.0 and ExoneraTor 4.4.0.
   - Switch from processing Torperf .tpf to OnionPerf analysis .json
     files.
   - Add Onion Service v3 statistics.
   - Add overloaded relay information by displaying an amber dot next
     to the relay nickname in relay search.
   - Update throughput calculation for Onionperf transfers.

 * Minor changes
   - Make Jetty host configurable.
   - Configure a base URL in order to turn ExoneraTor's permanent
     links into https:// links.
   - Set default locale `US` at the beginning of the execution.
   - Set default time zone `UTC` at the beginning of the execution.
   - Simplify logging configuration.


# Changes in version 1.3.0 - 2019-11-09

 * Medium changes
   - Start downloading and processing votes.
   - Add Apache Commons Math 3.6.1 as dependency.
   - Extend ipv6servers module to generate servers part of legacy
     module.
   - Use Ivy for resolving external dependencies rather than relying
     on files found in Debian stable packages. Requires installing Ivy
     (using `apt-get install ivy`, `brew install ivy`, or similar) and
     running `ant resolve` (or `ant -lib /usr/share/java resolve`).
     Retrieved files are then copied to the `lib/` directory, except
     for dependencies on other metrics libraries that still need to be
     copied to the `lib/` directory manually. Current dependency
     versions resolved by Ivy are the same as in Debian stretch with
     few exceptions.
   - Remove Cobertura from the build process.
   - Update PostgreSQL JDBC driver version to 42.2.5.
   - Update to metrics-lib 2.9.1 and ExoneraTor 4.2.0.


# Changes in version 1.2.0 - 2018-08-25

 * Medium changes
   - Add ExoneraTor 4.0.0 thin jar as dependency.


# Changes in version 1.1.0 - 2018-05-29

 * Medium changes
   - Replace Gson with Jackson.

 * Minor changes
   - Avoid sending an error after a (partial) response.


# Changes in version 1.0.3 - 2017-12-20

 * Major changes
   - Use an embedded Jetty.
   - Use metrics-base as build environment.
   - Add metrics timeline events underneath graphs.
   - Replace broken SVGs with higher-resolution PNGs.


# Changes in version 1.0.2 - 2017-10-04

 * Minor changes
   - Update news.json to version 147 of doc/MetricsTimeline.


# Changes in version 1.0.1 - 2017-09-25

 * Minor changes
   - Update link to old user number estimates.


# Changes in version 1.0.0 - 2017-09-19

 * Major changes
   - This is the initial release after almost eight years of
     development.
